from behave import register_type
from parse_type import TypeBuilder
import parse
from behave import given, when, then
from hamcrest import assert_that, equal_to, is_in

@parse.with_pattern(r"a\s+")
def parse_word_a(text):
    """Type converter for "a " (followed by one/more spaces)."""
    return text.strip()

# -- SAME:
# parse_optional_word_a = TypeBuilder.with_zero_or_one(parse_word_a)
parse_optional_word_a   = TypeBuilder.with_optional(parse_word_a)
register_type(optional_a_=parse_optional_word_a)
